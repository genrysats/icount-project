<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

  <style>
      * {
        font-family: Arial, Helvetica, sans-serif;
      }
  </style>
</head>

<body>
<div id='app' data-app>
<v-container fluid>
    <center>
      <v-col
        class="d-flex"
        cols="10"
        sm="5"
      >
        <v-select
          :items="countries"
          filled
          label="Choose a country"
          @change='chosenCountry'
        ></v-select>

        <v-select
          :items="networks"
          filled
          label="Choose a network"
          :disabled='networks.length == 0'
          v-model='network'
        >
        <template slot="item" slot-scope="data">
            {{ data.item.operator }}
        </template>

        <template slot="selection" slot-scope="data">
            {{ data.item.operator }}
        </template>
    </v-select>
      </v-col>


      <div v-for='key in Object.keys(network)'>
            {{ key }}: {{ network[key] }}
      </div>
    </center>
  </v-container>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js"></script>

<script>
new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  data: {
    countries: [],
    networks: [],
    network: ""
  },
  methods: {
    chosenCountry: function(country) {
        this.networks = [];
        this.network = "";
        
        axios.get(`api.php?country=${country}`).then(ans => {
        if(ans.data.success) {
            this.networks = ans.data.data;
        }else {
            console.log('err');
        }
    });
    },
  },
  mounted() {
    this.networks = [];
    this.network = "";
    //GET ALL COUNTRIES
    axios.get('api.php').then(ans => {
        if(ans.data.success) {
            this.countries = ans.data.data;
        }else {
            console.log('err');
        }
    });
  }
})
</script>

</body>
</html>
