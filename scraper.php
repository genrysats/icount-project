<?php 
set_time_limit(1000);

include_once('simple_html_dom.php');

$html = file_get_html('https://en.wikipedia.org/wiki/Mobile_country_code');

$table = $html->find('table.wikitable', 1);
$headers = ['country_code', 'country_name', 'mcc', 'mnc', 'brand', 'network_operator_name', 'status', 'bands'];
$data = [];

foreach ($table->find('tbody tr') as $row) {
    $all = $row->find('td');
    if(count($all) > 2)
    {
        $country_code = strip_tags($all[0]->innertext);
        $country_name = (strip_tags($all[1]->innertext));
        
        if(!is_array($all[3]->children) || count($all[3]->children) == 0) 
            continue;

        $link = 'https://en.wikipedia.org' . $all[3]->children[0]->href;
        $country_id = explode("#", $link)[1];

        $secondTable = file_get_html($link)->find("span[id=$country_id]", 0);
        if(!$secondTable) {
            continue;
        }
        $secondTable = $secondTable->parent()->next_sibling();
        
        foreach ($secondTable->find('tbody tr') as $row) {
            $elements = $row->find('td');

            if(count($elements) > 0) {
                $mcc = strip_tags($elements[0]->innertext);
                $mnc = strip_tags($elements[1]->innertext);
                $brand = strip_tags($elements[2]->innertext);
                $operator = strip_tags($elements[3]->innertext);
                $status = strip_tags($elements[4]->innertext);
                $bands = strip_tags($elements[5]->innertext);

                array_push($data, ['country_code' => trim($country_code), 'country_name' => trim($country_name), 'mcc' => $mcc, 'mnc' => $mnc, 'brand' => $brand, 'operator' => $operator, 'status' => $status, 'bands' => $bands]);
            }
        }

        echo 'row: ' . $country_name;
        
    }
}

file_put_contents('data.json', json_encode($data));


?>