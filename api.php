<?php 
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $json_obj = json_decode(file_get_contents("data.json"), true);

        if(isset($_GET['country'])) {
            //GET NETWORK BY COUNTRY
            $country_rows = [];
            foreach($json_obj as $row) {
                if(strcmp($row['country_name'], $_GET['country']) == 0) {
                    array_push($country_rows, $row);
                }
            }
            echo json_encode(['success' => true, 'data' => $country_rows]);
        }else {
            //GET ALL COUNTRIES
            $arr = array_values(array_unique(array_column($json_obj, 'country_name')));
            echo json_encode(['success' => true, 'data' => $arr]);
        }
        
    }
?>