-- 1 --
SELECT DISTINCT f.sku, f.description, f.quantity, s.quantity FROM items f, items s WHERE f.sku = s.sku AND ((f.warehouse = 'Warehouse 1' AND s.warehouse = 'Warehouse 2') OR (f.warehouse = 'Warehouse 2' AND s.warehouse = 'Warehouse 1' )) AND f.quantity >= s.quantity;

-- 2. --
SELECT * FROM items GROUP BY sku HAVING COUNT(*) = 1;
